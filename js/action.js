//mousedown
// mousemove
//mouseup

const dragElement = document.querySelector('.drag');
let offsetX = 0;
let offsetY = 0;

dragElement.addEventListener('mousedown', onMouseDown);
document.addEventListener('mouseup', onMouseUp);

function onMouseDown(event) {
    offsetX = event.offsetX;
    offsetY = event.offsetY;
    document.addEventListener('mousemove', onMouseMove);
    dragElement.classList.add('move');
}

function onMouseMove(event) {
    dragElement.style.left = event.pageX - offsetX + 'px';
    dragElement.style.top = event.pageY - offsetY + 'px';
}

function onMouseUp() {
    document.removeEventListener('mousemove', onMouseMove);
    dragElement.classList.remove('move');
}


//------------------------------------------------------

const wrapper = document.querySelector('.wrapper');
const modal = document.querySelector('.modal');
const button = document.querySelector('.close');
const inp = document.querySelector('input');

wrapper.addEventListener('click', closeModal);
modal.addEventListener('click', stopProp);
//button.addEventListener('click', closeModal);
inp.addEventListener('contextmenu', onContextMenu);
inp.addEventListener('keydown', onKeyDown);

function closeModal() {
    wrapper.style.display = 'none';
}

function stopProp(event) {
    event.stopPropagation(); // - остановка распространения события
}

function onContextMenu(event) {
    console.log('Context menu!');
    event.preventDefault(); // - отключает стандартное поведение
}

function onKeyDown(event) {
    if (event.key === 'g') {
        event.preventDefault();
        inp.value += 'Hello';
    }
}

//keypress
//keydown
//keyup

// ----------------- Stor -----------

// cookies
// session storage
// local storage
/*
JSON.stringify() // конвертирует в JSON строку для сохранения 
                // в localStorage или отправки на сервер

JSON.parse() // конвертация в JS код
*/
// web SQL
// index DB

button.addEventListener('click', saveToStorage);
const stor = localStorage.getItem('inputValue');
const parsStore = JSON.parse(stor);
console.log(parsStore.value);

function saveToStorage() {
    const text = inp.value;
    const toStore = {
        value: text
    };
    //localStorage.setItem('inputValue', text);
    localStorage.setItem('inputValue', JSON.stringify(toStore));
    inp.value = '';
}